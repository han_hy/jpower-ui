export default {
  tabs: true,
  tabsActive: 1,
  group: [{
    label: '个人信息',
    prop: 'info',
    // option: {
    //   submitText: '修改',
    //   size: 'small',
    //   labelWidth: 60,
      column: [{
        label: '头像',
        type: 'upload',
        listType: 'picture-img',
        loadText: '附件上传中，请稍等',
        propsHttp: {
          home: window.urllist.download,
          url: 'data'
        },
        canvasOption: {
          text: 'jpower',
          ratio: 0.1
        },
        action: window.urllist.update,
        tip: '只能上传jpg/png用户头像，且不超过500kb',
        span: 16,
        prop: 'avatarBase',
        accept: 'image/png,image/jpg'
      }, {
        label: '昵称',
        span: 12,
        prop: 'nickName'
      }, {
        label: '真实姓名',
        span: 12,
        prop: 'userName'
      }, {
        label: '部门',
        span: 24,
        prop: 'orgId',
        type: 'tree',
        dicUrl: window.home_url + '/jpower-system/core/org/tree',
        dataType:'string',
        props: {
          label: "title",
          value: "id"
        }
      }, {
        label: '证件类型',
        type: 'select',
        span: 12,
        prop: 'idType',
        dicUrl: window.urllist.dictUrl + 'ID_TYPE',
        dataType:'number',
        props: {
          label: "name",
          value: "code"
        }
      }, {
        label: '证件号码',
        span: 12,
        prop: 'idNo'

      },
      //   {
      //   label: '邮箱',
      //   span: 12,
      //   prop: 'email'
      //
      // },
        {
          label: '出生日期',
          type: 'date',
          span: 12,
          format: 'yyyy-MM-dd',
          valueFormat: 'yyyy-MM-dd',
          prop: 'birthday'
        }, {
        label: '邮编',
        row: true,
        span: 12,
        prop: 'postCode'
      }, {
        label: '地址',
        span: 24,
        prop: 'address'
      }]
    // }
  }, {
    label: '修改密码',
    prop: 'password',
    // option: {
    //   labelWidth: 70,
    //   size: 'small',
    //   submitText: '修改',
      column: [{
        label: '原密码',
        span: 16,
        row: true,
        type: 'password',
        prop: 'oldpassword',
        required: true
      }, {
        label: '新密码',
        span: 16,
        row: true,
        type: 'password',
        prop: 'newpassword',
        required: true
      }, {
        label: '确认密码',
        span: 16,
        row: true,
        type: 'password',
        prop: 'newpasswords',
        required: true
      }]
    // }
  }]
}