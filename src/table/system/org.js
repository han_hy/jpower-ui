
export default (safe)=>{
  //safe => vue的this对象
  return {
    lazy: true,
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    border: true,
    labelWidth: 130,
    searchLabelWidth: 130,
    viewBtn: true,
    menuWidth: 300,
    permission: {
      delCode: 'SYSTEM_ORG_DELETE',
      addCode: 'SYSTEM_ORG_ADD',
      editCode: 'SYSTEM_ORG_UPDATE'
    },
    column: [{
      label: "名称",
      prop: "name",
      search: true,
      rules: [{
        required: true,
        message: "请输入名称",
        trigger: "blur"
      }]
    },{
      label: "编码",
      prop: "code",
      search: true,
      rules: [{
        required: true,
        message: "请输入编码",
        trigger: "blur"
      },
        {
          pattern: /^[a-zA-Z0-9]{1,}$/,
          message: '只能输入数字或字母'
        }]
    },{
      label: "所属租户",
      prop: "tenantCode",
      type: 'tree',
      filterable: true,
      search: safe.$store.getters.isShowTenantCode,
      dicUrl: window.urllist.tenantSelectors,
      display: safe.$store.getters.isShowTenantCode,
      disabled: true,
      showColumn: safe.$store.getters.isShowTenantCode,
      props: {
        label: 'tenant_name',
        value: 'tenant_code'
      }
      },
      {
        label: "上级部门",
        prop: "parentId",
        showColumn: false,
        type: 'tree',
        dataType: 'string',
        filter: true,
        dicData: [],
        addDisabled: false,
        props: {
          label: 'title',
          value: 'key'
        },
        rules: [
          {
            required: false,
            message: "请选择上级部门",
            trigger: "click"
          }
        ]
      },
      {
        label: "联系人名字",
        search: true,
        prop: "contactName",
        rules: [
          {
            required: true,
            message: "请输入联系人名字",
            trigger: "blur"
          }
        ]

      },
      {
        label: '联系人邮箱',
        prop: 'contactEmail',
        showColumn: false,
        rules: [
          {
            message: "请输入联系人邮箱",
            trigger: "blur"
          },
          {
            pattern: /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/,
            message: "邮箱不合法"
          }
        ]
      },
      {
        label: "联系人电话",
        prop: "contactPhone",
        search: true,
        rules: [
          {
            required: true,
            message: "请输入联系人电话",
            trigger: "blur"
          },
          {
            pattern: /((([0-9]{3,4}-)?[0-9]{7,8}$)|(1[0-9]{10}$))/,
            message: "电弧不合法"
          }
        ]
      },
      {
        label: "领导人名字",
        prop: "headName",
        rules: [
          {
            message: "请输入领导人名字",
            trigger: "blur"
          }
        ]
      }, {
        label: "领导人电话",
        prop: "headPhone",
        showColumn: false,
        rules: [
          {
            message: "请输入领导人电话",
            trigger: "blur"
          },
          {
            pattern: /((([0-9]{3,4}-)?[0-9]{7,8}$)|(1[0-9]{10}$))/,
            message: "电话不合法"
          }
        ]
      },
      {
        label: '领导人邮箱',
        prop: 'headEmail',
        showColumn: false,
        rules: [
          {
            message: "请输入领导人邮箱",
            trigger: "blur"
          },
          {
            pattern: /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/,
            message: "邮箱不合法"
          }
        ]
      },
      {
        label: "虚拟机构 ",
        prop: "isVirtual",
        type: 'select',
        slot: true,
        dicUrl: window.urllist.dictUrl + 'YN01',
        dataType:'number',
        align: 'center',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否虚拟机构 ",
            trigger: "blur"
          }
        ]
      },
      {
        label: '排序',
        prop: 'sort',
        showColumn: false,
        type: 'number'
      },
      {
        label: "地址",
        prop: "address",
        type: 'textarea',
        showColumn: false,
        rules: [
          {
            message: "请输入地址",
            trigger: "blur"
          }
        ]
      },
      {
        label: '备注',
        prop: 'remark',
        showColumn: false,
        type: 'textarea'
      }
    ],
  }}