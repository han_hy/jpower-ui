export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    tree: true,
    border: true,
    treeProps: {
      hasChildren: 'has'
    },
    viewBtn: true,
    menuWidth: 430,
    permission: {
      setAuthCode: 'SYSTEM_ROLE_UPDATEFUNCTION',
      setDataAuthCode: 'SYSTEM_DATASCOPE_ROLE',
      addUserCode: 'SYSTEM_ROLE_ADDUSER',
      delUserCode: 'SYSTEM_ROLE_DELUSER',
      roleUserCode: 'SYSTEM_ROLE_USER',
      addCode: 'SYSTEM_ROLE_ADD',
      editCode: 'SYSTEM_ROLE_UPDATE',
      delCode: 'SYSTEM_ROLE_DELETE',
      viewCode: 'SYSTEM_ROLE_SELECT_URL'
    },
    column: [
      {
        label: "角色名称",
        prop: "name",
        search: true,
        span: 24,
        rules: [
          {
            required: true,
            message: "请输入角色名称",
            trigger: "blur"
          }
        ]
      },
      {
        label: "所属租户",
        prop: "tenantCode",
        type: 'tree',
        filterable: true,
        search: safe.$store.getters.isShowTenantCode,
        dicUrl: window.urllist.tenantSelectors,
        display: safe.$store.getters.isShowTenantCode,
        disabled: true,
        showColumn: safe.$store.getters.isShowTenantCode,
        props: {
          label: 'tenant_name',
          value: 'tenant_code'
        }
      },
      {
        label: "角色别名",
        prop: "alias",
        search: true,
        span: 24,
        rules: [
          {
            required: true,
            message: "请输入角色别名",
            trigger: "blur"
          }
        ]
      },
      {
        label: "上级角色",
        prop: "parentId",
        dicUrl: window.home_url + '/jpower-system/core/role/listTree',
        type: "tree",
        hide: true,
        span: 24,
        props: {
          label: "name",
          value: 'id'
        },
        rules: [
          {
            required: false,
            message: "请选择上级角色",
            trigger: "click"
          }
        ]
      },
      {
        label: "系统角色",
        prop: "isSysRole",
        type: 'select',
        dicUrl: window.urllist.dictUrl + 'YN01',
        dataType:'number',
        align: 'center',
        value: 0,
        search: true,
        tip: '系统角色无法删除，请谨慎选择',
        tipPlacement: 'top',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否系统角色",
            trigger: "blur"
          }
        ]
      },
      {
        label: "排序",
        prop: "sort",
        type: 'number',
        showColumn: false,
        precision: 0
      }, {
        label: '备注',
        type: 'textarea',
        span: 24,
        prop: 'remark',
        showColumn: false
      }

    ]
  }
}