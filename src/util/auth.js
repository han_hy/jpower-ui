import Cookies from 'js-cookie'
import store from '@/store/';
import { removeStore } from '@/util/store';
const TokenKey = window.$KEY.AUTH
export function getToken () {
  return Cookies.get(TokenKey)
}

export function setToken (token) {
  return Cookies.set(TokenKey, store.getters.tokenType+" "+token, { expires: new Date(new Date().getTime() + store.getters.expiresIn * 1000) });
}

export function removeToken () {
  removeStore({
    name: 'expiresIn'
  });
  return Cookies.remove(TokenKey);
}
