import { mapGetters } from "vuex";
import {isShowButton} from "@/api/showButton";
import {validatenull} from "@/util/validate";
export default (app, option = {}) => {
  let mixins = {
    data () {
      return {
        selection: [],
        data: [],
        form: {},
        params: {},
        api: require(`@/api${option.name}`),
        loading: false,
        page: {
          pageSizes: [10, 20, 30, 40, 50, 100, 1500, 3000],
          pageSize: 20
        },
        permission:{},
        isShowTenant: this.$store.getters.isShowTenantCode
      }
    },
    computed: {
      ...mapGetters(['userInfo', 'isDebug']),
      option(){
        return require(`@/table${option.name}`).default(this)
      },
      ids () {
        let ids = [];
        this.selection.forEach(ele => {
          ids.push(ele[this.rowKey]);
        });
        return ids.join(",");
      },
      bindVal () {
        return {
          ref: 'crud',
          option: this.option,
          data: this.data,
          tableLoading: this.loading,
          permission: this.getPermission
        }
      },
      onEvent () {
        return {
          'selection-change': this.selectionChange,
          'on-load': this.getList,
          'row-save': this.rowSave,
          'row-update': this.rowUpdate,
          'row-del': this.rowDel,
          'refresh-change': this.refreshChange,
          'date-change': this.dateChange,
          'search-reset': this.searchReset,
          'search-change': this.searchChange
        }
      },
      rowKey () {
        return this.option.rowKey || option.rowKey || 'id'
      }
    },
    created () {
      if(this.option.permission && this.$route.meta.id){
        Object.keys(this.option.permission).forEach(ele => {
          isShowButton(this.$route.meta.id, this.option.permission[ele]).then(res => {
            this.$set(this.permission,`${ele}Show`,res);
          });
        })
      }
    },
    methods: {
      getPermission (key,row) {

        if (this.customizePermission){
          return this.customizePermission(key,row);
        }

        if (key === 'editBtn') {
          return validatenull(this.permission.editCodeShow)?false:this.permission.editCodeShow;
        } if (key === 'addBtn') {
          return validatenull(this.permission.addCodeShow)?false:this.permission.addCodeShow;
        } else if (key === 'delBtn') {
          return validatenull(this.permission.delCodeShow)?false:this.permission.delCodeShow;
        } else if (key === 'viewBtn') {
          return validatenull(this.permission.viewCodeShow)?true:this.permission.viewCodeShow;
        } else if (key === 'excelBtn') {
          return validatenull(this.permission.excelCodeShow)?false:this.permission.excelCodeShow;
        } else {
          return true;
        }
      },
      getList () {
        const callback = () => {
          this.loading = true;
          let pageParams = {};

          if (option.crudPage !== false){
            pageParams[option.pageSize || 'pageSize'] = this.page.pageSize
            pageParams[option.pageNum || 'pageNum'] = this.page.currentPage
          }
          let data = Object.assign(pageParams, this.params)

          this.data = [];
          this.api[option.list || 'list'](data).then(res => {
            this.loading = false;
            let data;
            if (option.res) {
              data = option.res(res.data);
            } else {
              data = res.data.data
            }
            this.page.total = data[option.total || 'total'] || 0;
            const result = data[option.data || 'data'];
            this.data = result;
            if (this.listAfter) {
              this.listAfter(data)
            } else {
              this.$message.success('获取成功')
            }
          })
        }
        if (this.listBefore) {
          this.listBefore()
        }
        callback()
      },
      rowSave (row, done, loading) {
        const callback = () => {
          delete this.form.params;
          this.api[option.add || 'add'](this.form).then((data) => {
            this.getList();
            if (this.addAfter) {
              this.addAfter(data)
            } else {
              this.$message.success('新增成功')
            }
            done();
          }).catch(() => {
            if (loading) {
              loading();
            }else{
              done();
            }
          })
        }
        if (this.addBefore) {
          this.addBefore(callback)
        }else{
          callback()
        }
      },
      rowUpdate (row, index, done, loading) {
        const callback = () => {
          delete this.form.params;
          delete this.form.createOrg;
          delete this.form.createUser;
          delete this.form.createTime;
          delete this.form.updateTime;
          delete this.form.updateUser;

          this.api[option.update || 'update'](row[this.rowKey], this.form, index).then((data) => {
            this.getList();
            if (this.updateAfter) {
              this.updateAfter(data)
            } else {
              this.$message.success('更新成功')
            }
            done()
          }).catch(() => {
            loading()
          })
        }
        if (this.updateBefore) {
          this.updateBefore()
        }
        callback()
      },
      rowDel (row, index, done, loading) {
        const callback = () => {
          this.api[option.del || 'del'](row[this.rowKey], row).then((data) => {
            this.getList();
            if (this.delAfter) {
              this.delAfter(data, row, index)
            } else {
              this.$message.success('删除成功')
            }
            done();
          }).catch(() => {
            loading()
          })
        }
        if (this.delBefore) {
          this.delBefore(callback);
        } else {
          this.$confirm(`此操作将永久删除序号【${index+1}】的数据, 是否继续?`, '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            callback()
          })
        }
      },
      deletes () {

        if (this.ids.length <= 0){
          this.$message.warning("至少选中一条数据");
          return;
        }

        const callback = () => {
          this.$NProgress.start();
          this.api[option.del || 'del'](this.ids).then((data) => {
            this.$NProgress.done();
            this.getList();
            if (this.delAfter) {
              //TODO 批量删除暂时没传row和index数据，需要的时候再改
              this.delAfter(data, undefined, undefined)
            } else {
              this.$message.success('删除成功')
            }
          }).catch(() =>{
            this.$NProgress.done();
          })
        }
        if (this.delBefore) {
          this.delBefore(callback);
        } else {
          this.$confirm(`此操作将永久删除选中数据, 是否继续?`, '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            callback()
          })
        }
      },
      searchChange (params, done) {
        if (done) done();
        if (this.validatenull(params)) {
          Object.keys(this.params).forEach(ele => {
            if (!['createTime_dategt', 'createTime_datelt'].includes(ele)) {
              delete this.params[ele]
            }
          })
        } else {
          Object.keys(params).forEach(ele => {
            if (this.validatenull(params[ele])) {
              delete this.params[ele]
              delete params[ele];
            }
          })
        }
        this.params = Object.assign(this.params, params);
        this.page.currentPage = 1;
        if (this.searchBefore){
          this.searchBefore();
        }
        this.getList();
      },
      searchReset(params, done){
        if (this.searchResetAfter){
          this.searchResetAfter()
        }
        this.searchChange({}, done);
      },
      dateChange (date) {
        if (this.changeDate){
          this.changeDate(date);
        }else{
          // 默认是根据创建时间查询
          if (date) {
            this.params.createTime_dategt = date[0]
            this.params.createTime_datelt = date[1]
          } else {
            delete this.params.createTime_dategt
            delete this.params.createTime_datelt
          }
        }
        this.page.currentPage = 1;
        this.getList();
      },
      selectionChange (list) {
        this.selection = list;
      },
      refreshChange () {
        this.getList();
      }
    }
  }
  app.mixins = app.mixins || [];
  app.mixins.push(mixins)
  return app;
}