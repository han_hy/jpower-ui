
import request from '@/router/axios';
import { urllist } from '@/config/env';
let url = urllist.logs;
export const sendLogs = (data) => request({
  url: url + '/log',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const getList = (data) => request({
  url: url + '/log',
  method: 'get',
  meta: {
    isSerialize: true
  },
  params: data
})