
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.system

export const add = (data) => request({
  url: url + '/core/client/save',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})

export const update = (id, data) => request({
  url: url + '/core/client/save',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const del = (id) => request.delete(url + '/core/client/delete', {
  params: {
    ids: id
  }
})
export const list = (data) => request.get(url + '/core/client/list', {
  params: data
})