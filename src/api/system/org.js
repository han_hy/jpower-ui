
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system

export const list = (data) => request.get(url + '/core/org/listLazy', {
  params: data
})

export const listByparent = (data) => request.get(url + '/core/org/listLazyByParent', {
  params: data
})

export const add = (data) => request({
  url: url + '/core/org/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})

export const del = (id) => request.delete(url + '/core/org/deleteStatus', {
  params: {
    ids: id
  }
})
export const update = (id, data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/core/org/update',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: data
  })
}

export const selectTree = (data) => {
  return request({
    url: url + '/core/org/tree',
    method: 'get',
    params: data
  })
}

