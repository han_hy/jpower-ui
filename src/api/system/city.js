
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system

export const del = (id) => request.delete(url + '/core/city/delete', {
  params: {
    ids: id
  }
})
export const save = (data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  if(data.hasOwnProperty("pcode") && validatenull(data.pcode)){
    data.pcode = "-1";
  }

  return request({
    url: url + '/core/city/save',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const view = (id) => request.get(url + '/core/city/get', {
  params: {
    id
  }
})
export const list = (data) => request.get(url + '/core/city/lazyTree', {
  params: {
    ...{
      pcode: '-1'
    }, ...data
  }
})