
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.log

export const monitors = () => request.get(url + '/monitor/setting/monitors');

export const setup = (data) => request.get(url + '/monitor/setting/setup', {
    params: data
});

export const saveSetup = (data) => request({
    url: url + '/monitor/setting/save-setup',
    method: 'post',
    meta: {
        isSerialize: true
    },
    data: data
});

export const deleteSetup = (id) => request.delete(url + '/monitor/setting/delete-setup', {
    params: {
        id
    }
});

export const param = (data) => request.get(url + '/monitor/setting/param', {
    params: data
});

export const saveParam = (server,path,method,data) => request({
    url: url + '/monitor/setting/save-param',
    method: 'post',
    headers: {
        server: server,
        path: path,
        method: method,
    },
    data: data
});