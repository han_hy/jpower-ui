
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.log


export const list = (data) => request.get(url + '/monitor/log/list', {
  params: data
})


export const excel = (data) => request.get(url + '/monitor/log/export', {
  params: data
})

export const server = (data) => request.get(url + '/monitor/setting/servers', {
  params: data
})